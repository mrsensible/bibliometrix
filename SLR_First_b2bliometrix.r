library(bibliometrix)
load('bibliometrix.RData')

results <- biblioAnalysis(tidydf, sep = ";")



options(width=100)
S <- summary(object = results, k = 10, pause = FALSE)

pdf("general.pdf") 
plot(x = results, k = 15, pause = FALSE) 
dev.off() 

##
DF <- dominance(results, k = 10)
DF

##
authors=gsub(","," ",names(results$Authors)[1:10])
indices <- Hindex(tidydf, field = "author", elements=authors, sep = ";", years = 50)
indices$H

##
##
pdf("topauthor.pdf") 
topAU <- authorProdOverTime(tidydf, k = 15, graph = TRUE)
dev.off() 



# Create a country collaboration network

M <- metaTagExtraction(tidydf, Field = "AU_CO", sep = ";")
NetMatrix <- biblioNetwork(M, analysis = "collaboration", network = "countries", sep = ";")

# Plot the network
pdf("country_collab.pdf") 
net=networkPlot(NetMatrix, n = dim(NetMatrix)[1], Title = "Country Collaboration", type = "circle", size=TRUE, remove.multiple=FALSE,labelsize=.8,cluster="none")
dev.off() 


NetMatrix1 <- biblioNetwork(tidydf, analysis = "co-occurrences", network = "keywords", sep = ";")

# Plot the network
net1=networkPlot(NetMatrix1, normalize="association", weighted=T, n = 30, Title = "Keyword Co-occurrences", type = "fruchterman", size=T,edgesize = 5,labelsize=0.7)

pdf("../results/keywords.pdf") 
net1=networkPlot(NetMatrix1, normalize="association", weighted=T, n = 30, Title = "Keyword Co-occurrences", type = "fruchterman", size=T,edgesize = 5,labelsize=0.7)
dev.off() 


